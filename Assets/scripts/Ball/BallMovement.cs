﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour
{
    Rigidbody2D rigidbodyBall;
    float screenFactor;
    Vector3 startBallPos;
    bool isBounced = false;
    bool isFirstBounce = false;


    private void Start()
    {
        startBallPos = transform.position;
        screenFactor = (Camera.main.WorldToScreenPoint(GameController.Instance.redLine.transform.position).y / Screen.width);
        // screenFactor = ((float)Screen.height / Screen.width);
        rigidbodyBall = GetComponent<Rigidbody2D>();
        //GameController.Instance.SetStartPosition(gameObject);
        TrailStore.Instance.timeTrailEffect = GameController.Instance.trailEffect.time;
        TrailStore.Instance.startTimeTrailEffect = GameController.Instance.trailEffect.time;
    }

    void Update()
    {
        if (!GameController.Instance.isIntro)
        {
            Bounces();
            if (Input.GetMouseButtonDown(0) && GameController.Instance.isStart && !GameController.Instance.isGameOver && !GameController.Instance.IsMenuButtonClicked()
                && !GameController.Instance.menuPanel.activeSelf && !GameController.Instance.isMenu)
            {
                GameController.Instance.trailEffect.time = TrailStore.Instance.startTimeTrailEffect;
                rigidbodyBall.velocity = new Vector2(1f, Random.Range(-screenFactor, screenFactor)).normalized * MovementStore.Instance.ballSpeed;
                //rigidbodyBall.velocity = new Vector2(1f, screenFactor).normalized * MovementStore.Instance.ballSpeed;
                GameController.Instance.isStart = false;
                MovementStore.Instance.ballVelocity = rigidbodyBall.velocity;
                GameController.Instance.menuButton.gameObject.SetActive(false);
                GameController.Instance.pauseButton.SetActive(true);
                GameController.Instance.pauseButtonTop.SetActive(true);
                GameController.Instance.pauseButtonMid.SetActive(true);
            }
        }
    }

    /*private void FixedUpdate()
    {
        if (!GameController.Instance.isIntro)
        {
            Bounces();
            if (Input.GetMouseButtonDown(0) && GameController.Instance.isStart && !GameController.Instance.isGameOver && !GameController.Instance.IsMenuButtonClicked()
                && !GameController.Instance.menuPanel.activeSelf && !GameController.Instance.isMenu)
            {
                GameController.Instance.trailEffect.time = TrailStore.Instance.startTimeTrailEffect;
                rigidbodyBall.velocity = new Vector2(1f, Random.Range(-screenFactor, screenFactor)).normalized * MovementStore.Instance.ballSpeed;
                //rigidbodyBall.velocity = new Vector2(1f, screenFactor).normalized * MovementStore.Instance.ballSpeed;
                GameController.Instance.isStart = false;
                MovementStore.Instance.ballVelocity = rigidbodyBall.velocity;
                GameController.Instance.menuButton.gameObject.SetActive(false);
                GameController.Instance.pauseButton.SetActive(true);
            }
        }
    }*/

    private void OnCollisionEnter2D(Collision2D collision)
    {

        /* if (collision.gameObject.tag == "Wall vertical")
         {

             Camera.main.GetComponent<AudioSource>().PlayOneShot(AudioStore.Instance.wall);
             Vector2 currentVelocity = MovementStore.Instance.ballVelocity;
             Debug.Log("<color=blue>стена</color> ");
             rigidbodyBall.velocity = new Vector2(currentVelocity.x, -currentVelocity.y);
             MovementStore.Instance.ballVelocity = rigidbodyBall.velocity;

         }*/

        if (collision.gameObject.tag == "Player")
        {
            Camera.main.GetComponent<AudioSource>().PlayOneShot(AudioStore.Instance.pong);
            if (isFirstBounce)
            {
                isBounced = true;
            }
            MovementStore.Instance.BoostSpeed();
            GameController.Instance.trailEffect.time = MovementStore.Instance.startBallSpeed * TrailStore.Instance.startTimeTrailEffect / MovementStore.Instance.ballSpeed;
            float x = (transform.position.y - collision.gameObject.transform.position.y) / GameController.Instance.racketHeight;

            if (collision.gameObject.name == "Player")
            {
                rigidbodyBall.velocity = new Vector2(-1f, x).normalized * MovementStore.Instance.ballSpeed;
                Score.Instance.AddScore();
                Score.Instance.UpdateScoreToAdd();
                BotMovement.Instance.NextEpsilonY();
                MovementStore.Instance.isBallMoveLeft = true;

                Vector2 parallel = new Vector2(-1f, transform.position.y) - (Vector2)transform.position;
                if (rigidbodyBall.velocity.y > 0f)
                {
                    Vector2 maxTop = new Vector2(-7.5f, 3.1f) - (Vector2)transform.position;
                    if (Vector2.Angle(parallel, rigidbodyBall.velocity) < Vector2.Angle(parallel, maxTop))
                    {
                        BotMovement.Instance.isImposible = true;
                    }
                    //Debug.Log(Vector2.Angle(parallel, rigidbodyBall.velocity));
                    //Debug.Log("<color=red>qwe</color>" + Vector2.Angle(parallel, new Vector2(-8.8f, -3.5f) - (Vector2)transform.position));
                }
                else
                {
                    Vector2 maxBottom = new Vector2(-7.5f, -4.7f) - (Vector2)transform.position;
                    if (Vector2.Angle(parallel, rigidbodyBall.velocity) < Vector2.Angle(parallel, maxBottom))
                    {
                        BotMovement.Instance.isImposible = true;
                    }
                    //Debug.Log(Vector2.Angle(parallel, rigidbodyBall.velocity));
                    //Debug.Log("<color=blue>qwe</color>" + Vector2.Angle(parallel, new Vector2(-8.8f, -3.5f) - (Vector2)transform.position));
                }

            }
            else
            {
                BotMovement.Instance.isImposible = false;
                BotMovement.Instance.isImposibleMoving = false;
                BotMovement.Instance.isOnceChangeVelocity = true;
                rigidbodyBall.velocity = new Vector2(1f, x).normalized * MovementStore.Instance.ballSpeed;
                MovementStore.Instance.isBallMoveLeft = false;
            }
            MovementStore.Instance.ballVelocity = rigidbodyBall.velocity;
            TrailStore.Instance.timeTrailEffect = GameController.Instance.trailEffect.time;
        }

        if (collision.gameObject.tag == "Finish")
        {
            if (GameController.Instance.player.GetComponent<BoxCollider2D>().enabled == false)
            {
                GameController.Instance.player.GetComponent<BoxCollider2D>().enabled = true;
            }
            if (GameController.Instance.bot.GetComponent<BoxCollider2D>().enabled == false)
            {
                GameController.Instance.bot.GetComponent<BoxCollider2D>().enabled = true;
            }
            rigidbodyBall.velocity = Vector2.zero;
            MovementStore.Instance.ballVelocity = Vector2.zero;
            GameController.Instance.trailEffect.time = 0f;
            //GameController.Instance.SetStartPosition(gameObject);
            transform.position = startBallPos;
            GameController.Instance.isStart = true;
            if (collision.gameObject.name.Contains("Left"))
            {
                Camera.main.GetComponent<AudioSource>().PlayOneShot(AudioStore.Instance.win);
                MovementStore.Instance.isBallMoveLeft = false;
                Score.Instance.AddScore(10000);
                Score.Instance.UpdateScoreToAdd();
            }
            else
            {
                LifesController.Instance.RemoveLife();
                MovementStore.Instance.ResetBallSpeed();
                MovementStore.Instance.isBallMoveLeft = false;
                Score.Instance.ResetScoreToAdd();
            }
            GameController.Instance.SetStartPosition(GameController.Instance.bot);
            GameController.Instance.SetStartPosition(GameController.Instance.player);
            isFirstBounce = false;
            isBounced = false;
            BotMovement.Instance.isImposible = false;
            BotMovement.Instance.isImposibleMoving = false;
        }
    }


    public void Bounces()
    {
        if (transform.position.y > 3.1f || transform.position.y < -4.7f)
        {
            if (!isBounced)
            {
                Camera.main.GetComponent<AudioSource>().PlayOneShot(AudioStore.Instance.wall);
                Vector2 currentVelocity = MovementStore.Instance.ballVelocity;
                rigidbodyBall.velocity = new Vector2(currentVelocity.x, -currentVelocity.y);
                MovementStore.Instance.ballVelocity = rigidbodyBall.velocity;
                isBounced = true;
                isFirstBounce = true;
            }
        }
        if ((rigidbodyBall.velocity.y < 0f && transform.position.y < 0f || rigidbodyBall.velocity.y > 0f && transform.position.y > 0f) && isFirstBounce)
        {
            isBounced = false;
        }
    }
}
