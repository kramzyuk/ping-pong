﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotMovement : Singleton<BotMovement>
{

    Rigidbody2D rigidbodyBot;
    float startBotPositionY;
    float epsilonY;
    [HideInInspector]
    public bool isImposible = false;
    [HideInInspector]
    public bool isImposibleMoving = false;
    [HideInInspector]
    public bool isOnceChangeVelocity = true;

    void Start()
    {
        rigidbodyBot = GetComponent<Rigidbody2D>();
        //GameController.Instance.SetStartPosition(gameObject);
        startBotPositionY = transform.position.y;
    }


    /*void Update()
    {
        if (MovementStore.Instance.isBallMoveLeft)
        {
            if (isImposible)
            {
                if (isImposibleMoving)
                {
                    transform.position = new Vector3(transform.position.x, GameController.Instance.ball.transform.position.y);
                }
                else
                {
                    if (isOnceChangeVelocity)
                    {
                        rigidbodyBot.velocity = (new Vector2(transform.position.x, GameController.Instance.ball.transform.position.y) - (Vector2)transform.position).normalized * MovementStore.Instance.ballSpeed * 0.5f;
                        isOnceChangeVelocity = false;
                    }
                    if (rigidbodyBot.velocity.y > 0f && transform.position.y > GameController.Instance.ball.transform.position.y)
                    {
                        rigidbodyBot.velocity = Vector2.zero;
                        isImposibleMoving = true;
                    }
                    if (rigidbodyBot.velocity.y <= 0f && transform.position.y < GameController.Instance.ball.transform.position.y)
                    {
                        rigidbodyBot.velocity = Vector2.zero;
                        isImposibleMoving = true;
                    }
                }
            }
            else
            {
                rigidbodyBot.velocity = new Vector2(0f, MovementStore.Instance.ballVelocity.y).normalized * MovementStore.Instance.ballSpeed * 0.5f;
                if (rigidbodyBot.velocity.y > 0f && transform.position.y > GameController.Instance.ball.transform.position.y + epsilonY)
                {
                    rigidbodyBot.velocity = Vector2.zero;
                }
                if (rigidbodyBot.velocity.y <= 0f && transform.position.y < GameController.Instance.ball.transform.position.y + epsilonY)
                {
                    rigidbodyBot.velocity = Vector2.zero;
                }
            }
        }
        else
        {
            if (transform.position.y >= startBotPositionY - 0.1f && transform.position.y <= startBotPositionY + 0.1f)
            {
                rigidbodyBot.velocity = Vector2.zero;
            }
            else
            {
                rigidbodyBot.velocity = new Vector2(0f, startBotPositionY - transform.position.y).normalized * MovementStore.Instance.startBallSpeed * 0.5f;
            }
        }
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -4f, 2.3f));
    }*/


    private void FixedUpdate()
    {

        if (MovementStore.Instance.isBallMoveLeft)
        {
            if (isImposible)
            {
                if (isImposibleMoving)
                {
                    transform.position = new Vector3(transform.position.x, GameController.Instance.ball.transform.position.y);
                }
                else
                {
                    if (isOnceChangeVelocity)
                    {
                        rigidbodyBot.velocity = (new Vector2(transform.position.x, GameController.Instance.ball.transform.position.y) - (Vector2)transform.position).normalized * MovementStore.Instance.ballSpeed * 0.5f;
                        isOnceChangeVelocity = false;
                    }
                    if (rigidbodyBot.velocity.y > 0f && transform.position.y > GameController.Instance.ball.transform.position.y)
                    {
                        rigidbodyBot.velocity = Vector2.zero;
                        isImposibleMoving = true;
                    }
                    if (rigidbodyBot.velocity.y <= 0f && transform.position.y < GameController.Instance.ball.transform.position.y)
                    {
                        rigidbodyBot.velocity = Vector2.zero;
                        isImposibleMoving = true;
                    }
                }
            }
            else
            {
                rigidbodyBot.velocity = new Vector2(0f, MovementStore.Instance.ballVelocity.y).normalized * MovementStore.Instance.ballSpeed * 0.5f;
                if (rigidbodyBot.velocity.y > 0f && transform.position.y > GameController.Instance.ball.transform.position.y + epsilonY)
                {
                    rigidbodyBot.velocity = Vector2.zero;
                }
                if (rigidbodyBot.velocity.y <= 0f && transform.position.y < GameController.Instance.ball.transform.position.y + epsilonY)
                {
                    rigidbodyBot.velocity = Vector2.zero;
                }
            }
        }
        else
        {
            if (transform.position.y >= startBotPositionY - 0.1f && transform.position.y <= startBotPositionY + 0.1f)
            {
                rigidbodyBot.velocity = Vector2.zero;
            }
            else
            {
                rigidbodyBot.velocity = new Vector2(0f, startBotPositionY - transform.position.y).normalized * MovementStore.Instance.startBallSpeed * 0.5f;
            }
        }
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -4f, 2.3f));
    }

    public void NextEpsilonY()
    {
        epsilonY = Random.Range(GameController.Instance.racketHeight * (-0.5f), GameController.Instance.racketHeight * 0.5f);
    }
}
