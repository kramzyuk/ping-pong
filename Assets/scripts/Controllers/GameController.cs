﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : Singleton<GameController>
{
    [HideInInspector]
    public bool isIntro = false;
    [HideInInspector]
    public bool isStart = true;
    [HideInInspector]
    public bool isGameOver = false;
    [HideInInspector]
    public bool isMenu = false;
    public GameObject introPanel;
    public GameObject menuPanel;
    public GameObject gameOverPanel;
    public GameObject highscorePanel;
    public GameObject redLine;
    public GameObject ball;
    public GameObject bot;
    public GameObject player;
    public GameObject highscoresContent;
    public Button menuButton;
    public GameObject pauseButton;
    public GameObject pauseButtonTop;
    public GameObject pauseButtonMid;
    public float racketHeight;
    [HideInInspector]
    public float maxRacketPosY, minRacketPosY;
    public TrailRenderer trailEffect;
    Vector3 menuButtonArea;
    float menuButtonWidth;
    float menuButtonHeight;
    Vector3 pauseTopArea, pauseCenterArea;
    float pauseTopHeight, pauseCenterHeight;


    float startPosY;

    private void Start()
    {
        if (PlayerPrefs.GetInt("isNewGame") == 1)
        {
            introPanel.SetActive(false);
            isIntro = false;
            PlayerPrefs.SetInt("isNewGame", 0);
        }
        //    PlayerPrefs.DeleteAll();
        SetHighscores();
        maxRacketPosY = 2.35f;
        minRacketPosY = -3.6f;
        startPosY = (minRacketPosY + maxRacketPosY) / 2f;
        menuButtonArea = Camera.main.WorldToScreenPoint(menuButton.gameObject.transform.position);
        pauseTopArea = Camera.main.WorldToScreenPoint(pauseButtonTop.transform.position);
        pauseCenterArea = Camera.main.WorldToScreenPoint(pauseButtonMid.transform.position);
        menuButtonWidth = menuButton.gameObject.GetComponent<RectTransform>().rect.width;
        menuButtonHeight = menuButton.gameObject.GetComponent<RectTransform>().rect.height;
        pauseTopHeight = pauseButtonTop.GetComponent<RectTransform>().rect.height;
        pauseCenterHeight = pauseButtonMid.GetComponent<RectTransform>().rect.height;
    }


    private void Update()
    {
        if (isIntro)
        {
            if (Input.GetMouseButtonUp(0))
            {
                introPanel.SetActive(false);
                isIntro = false;
            }
        }
    }


    public bool IsMenuButtonClicked()
    {
        if ((Input.mousePosition.x >= menuButtonArea.x - menuButtonWidth / 2 && Input.mousePosition.x <= menuButtonArea.x + menuButtonWidth / 2) &&
            (Input.mousePosition.y >= menuButtonArea.y - menuButtonHeight / 2 && Input.mousePosition.y <= menuButtonArea.y + menuButtonHeight / 2))
        {
            return true;
        }
        return false;
    }

    public bool IsPauseClicked()
    {
        if (Input.mousePosition.y >= pauseTopArea.y - pauseTopHeight / 2 ||
            Input.mousePosition.x >= pauseCenterArea.x - pauseCenterHeight / 2 && Input.mousePosition.x <= pauseCenterArea.x + pauseCenterHeight / 2 &&
            Input.mousePosition.y >= pauseCenterArea.y - pauseCenterHeight / 2 && Input.mousePosition.y <= pauseCenterArea.y + pauseCenterHeight / 2)
            return true;

        return false;
    }

    public void GameOver()
    {
        Camera.main.GetComponent<AudioSource>().PlayOneShot(AudioStore.Instance.gameOver);
        Score.Instance.SetGameOverScore();
        gameOverPanel.SetActive(true);
        isGameOver = true;
        SaveHighscoresInApp();
        SetHighscores();
    }

    public void SetStartPosition(GameObject obj)
    {
        if (obj == ball)
        {
            obj.transform.position = new Vector3(0f, startPosY);
            return;
        }
        obj.transform.position = new Vector3(obj.transform.position.x, startPosY);
    }


    public void NewGame()
    {
        PlayerPrefs.SetInt("isNewGame", 1);
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void SeeHighscores()
    {
        isMenu = true;
        menuPanel.SetActive(false);
        gameOverPanel.SetActive(false);
        highscorePanel.SetActive(true);
    }

    public void CloseHighscores()
    {
        highscorePanel.SetActive(false);
        if (isGameOver)
        {
            gameOverPanel.SetActive(true);
        }
        else
        {
            menuPanel.SetActive(true);
        }
    }

    public void CloseGame()
    {
        menuPanel.SetActive(false);
        isMenu = false;
    }


    public void ResumeGame()
    {
        Time.timeScale = 1;
        menuPanel.SetActive(false);
        isMenu = false;
    }

    public void MenuButton()
    {
        isMenu = true;
        menuPanel.SetActive(true);
        menuPanel.transform.Find("Close").gameObject.SetActive(true);
        menuPanel.transform.Find("Resume game").gameObject.SetActive(false);
    }

    public void PauseButton()
    {
        isMenu = true;
        Time.timeScale = 0;
        menuPanel.SetActive(true);
        menuPanel.transform.Find("Close").gameObject.SetActive(false);
        menuPanel.transform.Find("Resume game").gameObject.SetActive(true);
        Score.Instance.ResetScoreToAdd();
    }

    public void SaveHighscoresInApp()
    {
        for (int i = 0; i < 10; i++)
        {
            if (PlayerPrefs.GetString("date" + i) != "")
            {
                if (Score.Instance.score >= PlayerPrefs.GetInt("score" + i))
                {
                    for (int k = 9; k > i; k--)
                    {
                        PlayerPrefs.SetInt("score" + k, PlayerPrefs.GetInt("score" + (k - 1)));
                        PlayerPrefs.SetString("date" + k, PlayerPrefs.GetString("date" + (k - 1)));
                    }
                    PlayerPrefs.SetInt("score" + i, Score.Instance.score);
                    PlayerPrefs.SetString("date" + i, System.DateTime.Now.ToString("dd.MM.yyyy - HH:mm"));
                    return;
                }
            }
            else if (PlayerPrefs.GetString("date" + i) == "")
            {
                PlayerPrefs.SetInt("score" + i, Score.Instance.score);
                PlayerPrefs.SetString("date" + i, System.DateTime.Now.ToString("dd.MM.yyyy - HH:mm"));
                return;
            }
        }
    }


    public void SetHighscores()
    {
        for (int i = 0; i < 10; i++)
        {
            if (PlayerPrefs.GetString("date" + i) != "")
            {

                highscoresContent.transform.GetChild(i).gameObject.SetActive(true);
                highscoresContent.transform.GetChild(i).transform.GetChild(0).GetComponent<Text>().text = PlayerPrefs.GetInt("score" + i).ToString();
                highscoresContent.transform.GetChild(i).transform.GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString("date" + i).ToString();
            }
        }
        highscoresContent.GetComponent<RectTransform>().offsetMin = new Vector2(
            highscoresContent.GetComponent<RectTransform>().offsetMin.x,
            -(100 + 73 * GetCountHighscores()) + highscoresContent.GetComponent<RectTransform>().offsetMax.y);
    }

    public int GetCountHighscores()
    {
        for (int i = 0; i < 10; i++)
        {
            if (!highscoresContent.transform.GetChild(i).gameObject.activeSelf)
            {
                return i - 1;
            }
        }
        return 9;
    }

    public void WebsiteClicked()
    {
        Application.OpenURL("http://disto.com");
    }

    public void FacebookClicked()
    {
        Application.OpenURL("https://www.facebook.com/LeicaGeosystems");
    }
}
