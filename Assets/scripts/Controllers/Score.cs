﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : Singleton<Score> {

    public int score;
    public Text scoreText;
    public Text gameOverScoreText;
    int scoreToAdd;

    private void Start()
    {
        scoreToAdd = 300;
    }

    public void SetScore()
    {
        scoreText.text = score.ToString();
    }


    public void AddScore()
    {
        score += scoreToAdd;
        SetScore();
    }

    public void AddScore(int add)
    {
        score += add;
        SetScore();
    }

    public void UpdateScoreToAdd()
    {
        scoreToAdd += 10 ;
    }

    public void UpdateScoreToAdd(int add)
    {
        scoreToAdd += add;
    }


    public void ResetScoreToAdd()
    {
        scoreToAdd = 300;
    }

    public void SetGameOverScore()
    {
        gameOverScoreText.text = "You achieved " + score.ToString() + " Points!";
    }
}
