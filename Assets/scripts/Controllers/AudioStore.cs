﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStore : Singleton<AudioStore>
{

    public AudioClip win;
    public AudioClip lose;
    public AudioClip gameOver;
    public AudioClip pong;
    public AudioClip wall;


}
