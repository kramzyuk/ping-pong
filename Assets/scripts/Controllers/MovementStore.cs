﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementStore : Singleton<MovementStore>
{

    public Vector2 ballVelocity;
    public float ballSpeed;
    public float maxBallSpeed;
    public bool isBallMoveLeft = false;
    public float startBallSpeed;
    public float boostBallSpeed;
    float screenFactor;


    private void Start()
    {
        screenFactor = (float)Screen.width / Screen.height;
        ballSpeed *= screenFactor / 1.77777f;
        startBallSpeed = ballSpeed;
        boostBallSpeed = ballSpeed * 0.05f;
    }

    public void BoostSpeed()
    {
        if (ballSpeed < maxBallSpeed)
        {
            ballSpeed += boostBallSpeed;
        }
    }

    public void ResetBallSpeed()
    {
        ballSpeed = startBallSpeed;
    }

}
