﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifesController : Singleton<LifesController>
{
    public int playerLifesCount = 3;
    public GameObject playerLifes;

    public void RemoveLife()
    {
        if (playerLifesCount != 0)
        {
            playerLifesCount--;
            playerLifes.transform.GetChild(playerLifesCount).gameObject.SetActive(false);
            if (playerLifesCount == 0)
            {
                GameController.Instance.GameOver();
                return;
            }
            Camera.main.GetComponent<AudioSource>().PlayOneShot(AudioStore.Instance.lose);
        }
    }

    public void ResetLifes()
    {
        for (int i = 0; i < 3; i++)
        {
            playerLifes.transform.GetChild(i).gameObject.SetActive(true);
        }
    }



}
