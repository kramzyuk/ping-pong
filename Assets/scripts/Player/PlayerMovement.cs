﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    Vector3 mousePos;


    private void Start()
    {
        //GameController.Instance.SetStartPosition(gameObject);
    }

    private void Update()
    {
        if (Input.GetMouseButton(0) && !GameController.Instance.isIntro && !GameController.Instance.isGameOver &&
            !GameController.Instance.isMenu && !GameController.Instance.IsMenuButtonClicked() && !GameController.Instance.IsPauseClicked())
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(transform.position.x, mousePos.y);
        }
        //transform.position = new Vector3(transform.position.x, GameController.Instance.ball.transform.position.y);
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -4f, 2.3f));
    }

    /*
    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0) && !GameController.Instance.isIntro && !GameController.Instance.isGameOver &&
            !GameController.Instance.isMenu && !GameController.Instance.IsMenuButtonClicked() && !GameController.Instance.IsPauseClicked())
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(transform.position.x, mousePos.y)
        }
        //transform.position = new Vector3(transform.position.x, GameController.Instance.ball.transform.position.y);
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -4f, 2.3f));
    }*/

}
